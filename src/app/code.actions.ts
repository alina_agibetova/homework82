import { createAction, props } from '@ngrx/store';

export const addChar = createAction(
  '[Code] Add character'
);

export const minusChar = createAction(
  '[Code] Minus character'
);
export const addOne = createAction(
  '[Code] Add one',
  props<{char: string}>());

export const addTwo =
  createAction(
    '[Code] Add two',
    props<{char: string}>());

export const addThree =
  createAction(
    '[Code] Add three',
    props<{char: string}>());

export const addFour =
  createAction(
    '[Code] Add four',
    props<{char: string}>());

export const addFive =
  createAction(
    '[Code] Add five',
    props<{char: string}>());

export const addSix =
  createAction(
    '[Code] Add six',
    props<{char: string}>());

export const addSeven =
  createAction(
    '[Code] Add seven',
    props<{char: string}>());

export const addEight =
  createAction(
    '[Code] Add eight',
    props<{char: string}>());

export const addNine =
  createAction(
    '[Code] Add nine',
    props<{char: string}>());

export const addZero =
  createAction(
    '[Code] Add zero',
    props<{char: string}>());
