import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import {
  addChar,
  addEight,
  addFive,
  addFour,
  addNine,
  addOne,
  addSeven,
  addSix,
  addThree,
  addTwo,
  addZero,
  minusChar
} from '../code.actions';


@Component({
  selector: 'app-code',
  templateUrl: './code.component.html',
  styleUrls: ['./code.component.css']
})
export class CodeComponent {
  code!: Observable<string>;
  password!: string[];
  numbers: string[] = [];
  isCorrect!: boolean;

  constructor(private store: Store<{code: string}>) {
    this.code = this.store.select('code');
    this.code.subscribe( (item: any) => {
      this.password = item.password.split('');
      this.isCorrect = item.isCorrect;
      console.log(this.isCorrect);
    })
  }

  addChar(){
    this.store.dispatch(addChar());
  }

  minusChar() {
    this.store.dispatch(minusChar());
  }

  addOne(){
    this.store.dispatch(addOne({char: '1'}));
  }
  addTwo(){
    this.store.dispatch(addTwo({char: '2'}))
  }
  addThree(){
    this.store.dispatch(addThree({char: '3'}))
  }
  addFour(){
    this.store.dispatch(addFour({char: '4'}))
  }
  addFive(){
    this.store.dispatch(addFive({char: '5'}))
  }
  addSix(){
    this.store.dispatch(addSix({char: '6'}))
  }
  addSeven(){
    this.store.dispatch(addSeven({char: '7'}))
  }
  addEight(){
    this.store.dispatch(addEight({char: '8'}))
  }
  addNine(){
    this.store.dispatch(addNine({char: '9'}))
  }
  addZero(){
    this.store.dispatch(addZero({char: '0'}))
  }
}
