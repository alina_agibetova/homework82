import { createReducer, on } from '@ngrx/store';
import {
  addChar,
  addEight,
  addFive,
  addFour, addNine,
  addOne,
  addSeven,
  addSix,
  addThree,
  addTwo, addZero,
  minusChar
} from './code.actions';


const correctPassword = '1337';
const initialState = {
  isCorrect: false,
  password: ''
};

export const codeReducer = createReducer(
  initialState,
  on(addChar, (state)=> {
    if (state.password === correctPassword){
      return {...state, isCorrect: true}
    }
    return {...state, isCorrect: false};
  }),

  on(minusChar, (state) => {
    if(state.password.length <= 0){
      return state;
    }
    const newPass = state.password.slice(0, state.password.length -1);
    return {...state, password: newPass};
  }),

  on(addOne, (state, {char}) => {
    const newPassword = state.password + char;
    return {...state, password: newPassword};
  }),

  on(addTwo, (state, {char}) => {
    const newPassword = state.password + char;
    return  {...state, password: newPassword};
  }),
  on(addThree, (state, {char}) =>{
    const newPassword = state.password + char;
    return  {...state, password: newPassword};

  }),
  on(addFour, (state, {char}) =>{
    const newPassword = state.password + char;
    return  {...state, password: newPassword};
  }),
  on(addFive, (state, {char}) =>{
    const newPassword = state.password + char;
    return  {...state, password: newPassword};
  }),
  on(addSix, (state, {char}) =>{
    const newPassword = state.password + char;
    return  {...state, password: newPassword};
  }),
  on(addSeven, (state, {char}) =>{
    const newPassword = state.password + char;
    return  {...state, password: newPassword};
  }),
  on(addEight, (state, {char}) =>{
    const newPassword = state.password + char;
    return  {...state, password: newPassword};
  }),
  on(addNine, (state, {char}) =>{
    const newPassword = state.password + char;
    return  {...state, password: newPassword};
  }),
  on(addZero, (state, {char}) =>{
    const newPassword = state.password + char;
    return  {...state, password: newPassword};
  })
);
